Ansible Tableau Role
========
Ansible role to install tableau on Ubuntu 16.04

Role Variables
--------------
- `tableau_version_major`: Major version, default 10
- `tableau_version_minor`: The minor version, default 5
- `tableau_version_patch`: The patch, default 0
- `tableau_enable_self_signed_tls`: Whether to enable SSL with a self-signed certificate, defaults to false. This is useful if TLS is terminated elsewhere.
- `tableau_hostname`: defaults to localhost
- `tableau_postgresql_odbc_package`: defaults to tableau-postgresql-odbc_9.5.3_amd64.deb
- `tableau_user`: defaults to tabadmin
- `tableau_password`: needs to be specified, ideally not on a configuration file
- `tsm_licence`: the tableau license
- `tsm_registration`: the registration details as per below:
```
  zip: 03079
  country: USA
  city: Salem
  last_name: Smith
  industry: Software
  eula: yes
  title: Software Applications Engineer
  phone: 5556875309
  company: Example
  state: NH
  department: Engineering
  first_name: Jason
  email: jsmith@example.com
```
- `tsm_identity_store_type`: defaults to local
- `tabcmd_admin_username`: defaults to admin
- `tabcmd_admin_password`: needs to be specified
